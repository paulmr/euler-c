#include <stdio.h>
#include <stdlib.h>

/* http://projecteuler.net/problem=1:
 *
 *   "If we list all the natural numbers below 10 that are multiples of 3 or 5,
 *   we get 3, 5, 6 and 9. The sum of these multiples is 23.
 *
 *   Find the sum of all the multiples of 3 or 5 below 1000."
 */

/* is x a multiple of n? */
int ismult(int n, int x) {
    return (x % n) == 0;
}

/* is x a multiple of any of the values in n[]? */
int anymult(int *n, int amt, int x) {
    int i = amt - 1;
    while(i >= 0) {
        if (ismult(n[i--], x)) {
            return 1;
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    int i, sum = 0, limit = 10;
    int *div = NULL;
    int divcount = 0;

    if (argc > 1) {
        sscanf(argv[1], "%d", &limit);
    }

    if (argc > 2) {
        divcount = argc - 2;
        div = malloc(sizeof(int) * divcount);
        if (div == NULL) {
            perror("alloc'ing memory");
            exit(1);
        }
        for (i = 0; i < divcount; i++) {
            div[i] = atoi(argv[i + 2]);
        }
    }

    for (i = 1; i < limit; i++) {
        if (anymult(div, divcount, i)) {
            sum += i;
        }
    }

    printf("sum: %d\n", sum);
}
